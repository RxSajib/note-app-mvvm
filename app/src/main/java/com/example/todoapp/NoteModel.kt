package com.example.todoapp

import androidx.annotation.NonNull
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "NoteDB")
data class NoteModel(
    @PrimaryKey(autoGenerate = false)
    var id: Int,
    var title : String,
    var details : String,
    var active : Boolean,
    var themes : String
)
