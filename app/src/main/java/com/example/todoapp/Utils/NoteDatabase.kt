package com.example.todoapp.Utils

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.todoapp.NoteModel

@Database(entities = [NoteModel::class], version = 6)
abstract class NoteDatabase : RoomDatabase() {

    abstract fun dao() : Dao

    companion object{
        @Volatile
        private var INSTANT : NoteDatabase ?= null

        fun getDatabase(context: Context) : NoteDatabase{
            val tempinstat = INSTANT
            if(tempinstat != null){
                return tempinstat
            }
            synchronized(this){
                val instant = Room.databaseBuilder(context.applicationContext,
                NoteDatabase::class.java, "note").fallbackToDestructiveMigration().build()

                INSTANT = instant
                return instant
            }
        }
    }
}