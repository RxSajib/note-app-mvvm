package com.example.todoapp.Utils

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.todoapp.NoteModel

@Dao
interface Dao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun AddNote(noteModel: NoteModel)

    @Query("SELECT * FROM notedb")
    fun GetAllNote() : LiveData<List<NoteModel>>

    @Query("UPDATE notedb SET active = :Val WHERE ID = :ID")
   suspend fun UpdateNote(Val : Boolean, ID : Int)
}