package com.example.todoapp.UI.ViewHolder

import android.view.View
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.example.todoapp.databinding.NoteitemBinding

class NoteViewHolder(itemView: NoteitemBinding) : ViewHolder(itemView.root) {

    public var item: NoteitemBinding

    init {
        item = itemView;
    }

}