package com.example.todoapp.UI.Adapter

import android.graphics.Color
import android.graphics.Paint
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CompoundButton
import android.widget.Toast
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView.Adapter
import androidx.room.Insert
import com.example.todoapp.NoteModel
import com.example.todoapp.R
import com.example.todoapp.UI.ViewHolder.NoteViewHolder
import com.example.todoapp.databinding.NoteitemBinding
import javax.inject.Inject
import javax.inject.Singleton


@Singleton
class  NoteAdapter @Inject constructor() : ListAdapter<NoteModel, NoteViewHolder>(DIFFUTILS())  {


   open class DIFFUTILS : DiffUtil.ItemCallback<NoteModel>() {
        override fun areItemsTheSame(oldItem: NoteModel, newItem: NoteModel): Boolean {
           return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: NoteModel, newItem: NoteModel): Boolean {
           return oldItem.title.equals(newItem.title) &&  oldItem.details.equals( newItem.details)
                   && oldItem.active == newItem.active;
        }
    }


    private lateinit var click  : OnClick;
    private  val TAG = "NoteAdapter"




    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NoteViewHolder {
        var  l = LayoutInflater.from(parent.context)
        var v = NoteitemBinding.inflate(l, parent, false)
        return NoteViewHolder(v)
    }


    override fun onBindViewHolder(holder: NoteViewHolder, position: Int) {
        holder.item.note = getItem(position)

        var active = getItem(position).active
        if(!active){
            holder.item.TitleNote.paintFlags =   holder.item.TitleNote.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
            holder.item.DetailsNote.paintFlags =   holder.item.DetailsNote.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG

            holder.item.SwitchButton.setBackgroundResource(R.drawable.offbg)

        }else{
            holder.item.TitleNote.paintFlags = 0
            holder.item.DetailsNote.paintFlags = 0

            holder.item.SwitchButton.setBackgroundResource(R.drawable.onbg)
        }

        holder.item.SwitchButton.setOnClickListener(View.OnClickListener {
            click.Click(getItem(position), true)
        })





        if(getItem(position).themes.equals("White")){
            holder.item.Root.setBackgroundResource(R.drawable.whitebg)
            holder.item.TitleNote.setTextColor(holder.itemView.context.resources.getColor(R.color.black))
            holder.item.DetailsNote.setTextColor(holder.itemView.context.resources.getColor(R.color.black))
        }

        if(getItem(position).themes.equals("Black")){
            holder.item.Root.setBackgroundResource(R.drawable.blackbg)
            holder.item.TitleNote.setTextColor(holder.itemView.context.resources.getColor(R.color.white))
            holder.item.DetailsNote.setTextColor(holder.itemView.context.resources.getColor(R.color.white))
        }
        if(getItem(position).themes.equals("Red")){
            holder.item.Root.setBackgroundResource(R.drawable.redbg)
            holder.item.TitleNote.setTextColor(holder.itemView.context.resources.getColor(R.color.white))
            holder.item.DetailsNote.setTextColor(holder.itemView.context.resources.getColor(R.color.white))
        }
        if(getItem(position).themes.equals("Green")){
            holder.item.Root.setBackgroundResource(R.drawable.greenbg)
            holder.item.TitleNote.setTextColor(holder.itemView.context.resources.getColor(R.color.white))
            holder.item.DetailsNote.setTextColor(holder.itemView.context.resources.getColor(R.color.white))
        }

    }


    public interface OnClick{
        fun Click(noteModel: NoteModel, state : Boolean)
    }

    public fun ClickState(click: OnClick){
        this.click = click
    }
}