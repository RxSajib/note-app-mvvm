package com.example.todoapp.UI

import android.os.Bundle
import android.view.Gravity
import android.view.View
import android.view.WindowManager
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.todoapp.Application.Application
import com.example.todoapp.NoteModel
import com.example.todoapp.R
import com.example.todoapp.Repository.ViewModel
import com.example.todoapp.UI.Adapter.NoteAdapter
import com.example.todoapp.databinding.NoteBinding
import com.example.todoapp.databinding.NotebottomsheetBinding
import com.google.android.material.bottomsheet.BottomSheetDialog
import java.util.Random
import javax.inject.Inject


class Note : AppCompatActivity() {

    private lateinit var noteBinding: NoteBinding;
    private lateinit var viewModel: ViewModel
   @Inject lateinit var noteAdapter: NoteAdapter
    private var Background : String = "White"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        noteBinding = DataBindingUtil.setContentView(this, R.layout.note);
        viewModel = ViewModelProvider(this).get(ViewModel::class.java)


        var inject = (application as Application).component
        inject.InjectNote(this)





        viewModel.GetAllNote().observe(this, Observer {
            noteAdapter.submitList(it)

            if(it.size > 0){
                noteBinding.NoteIcon.visibility = View.GONE
                noteBinding.Message.visibility = View.GONE
            }else{
                noteBinding.NoteIcon.visibility = View.VISIBLE
                noteBinding.Message.visibility = View.VISIBLE
            }
        })

        InitView();
        AddNote();
    }


    private fun AddNote(){
        noteBinding.FloatingActionButton.setOnClickListener {
             Background = "White"
            var dialog = BottomSheetDialog(this)
            var d = NotebottomsheetBinding.inflate(layoutInflater, null, false)
            dialog.setContentView(d.root)
            dialog.show()
            val params = window.attributes
            params.width = WindowManager.LayoutParams.MATCH_PARENT
            params.height = WindowManager.LayoutParams.MATCH_PARENT
            params.gravity = Gravity.CENTER
            window.attributes = params
            window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE );




            d.WhiteBtn.setOnClickListener {
                d.WhiteIcon.setImageResource(R.drawable.done)
                d.BlackIcon.setImageResource(R.drawable.empty)
                d.RedIcon.setImageResource(R.drawable.empty)
                d.GreenIcon.setImageResource(R.drawable.empty)
                Background = "White"
            }

            d.BlackBtn.setOnClickListener {
                d.WhiteIcon.setImageResource(R.drawable.empty)
                d.BlackIcon.setImageResource(R.drawable.done)
                d.RedIcon.setImageResource(R.drawable.empty)
                d.GreenIcon.setImageResource(R.drawable.empty)
                Background = "Black"
            }

            d.RedBtn.setOnClickListener {
                d.WhiteIcon.setImageResource(R.drawable.empty)
                d.BlackIcon.setImageResource(R.drawable.empty)
                d.RedIcon.setImageResource(R.drawable.done)
                d.GreenIcon.setImageResource(R.drawable.empty)
                Background = "Red"
            }

            d.GreenBtn.setOnClickListener {
                d.WhiteIcon.setImageResource(R.drawable.empty)
                d.BlackIcon.setImageResource(R.drawable.empty)
                d.RedIcon.setImageResource(R.drawable.empty)
                d.GreenIcon.setImageResource(R.drawable.done)
                Background = "Green"
            }




            d.AddNoteBtn.setOnClickListener {
                var title = d.TitleID.text.toString().trim()
                var details = d.DetailsID.text.toString().trim()

                if(title.isEmpty()){
                    d.TitleID.error = "Title empty"
                }else if(details.isEmpty()){
                    d.DetailsID.error = "Details empty"
                }else{
                    val rnd = Random()
                    viewModel.AddNote(NoteModel( rnd.nextInt(999999), title, details, true, Background))
                    Toast.makeText(applicationContext, "Note added success", Toast.LENGTH_LONG).show()
                    dialog.dismiss()

                }
            }

        }
    }

    private fun InitView(){
        noteBinding.RecyclerView.apply {
            this.setHasFixedSize(true)
            this.adapter = noteAdapter
        }

        noteAdapter.ClickState(object : NoteAdapter.OnClick {
            override fun Click(noteModel: NoteModel, state: Boolean) {
                if(noteModel.active){
                    viewModel.UpdateNote(false, noteModel.id)
                }else{
                    viewModel.UpdateNote(true, noteModel.id)
                }
            }
        })

    }

}