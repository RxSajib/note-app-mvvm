package com.example.todoapp.Repository

import androidx.lifecycle.LiveData
import com.example.todoapp.NoteModel
import com.example.todoapp.Utils.Dao

class Repository(private val dao: Dao) {

    val readallnote : LiveData<List<NoteModel>>  = dao.GetAllNote()

    suspend fun AddNote(noteModel: NoteModel){
        dao.AddNote(noteModel)
    }

    fun GetAllNote() : LiveData<List<NoteModel>>{
       return dao.GetAllNote()
    }

    suspend fun UpdateNote(State : Boolean, ID : Int){
        dao.UpdateNote(State, ID)
    }
}