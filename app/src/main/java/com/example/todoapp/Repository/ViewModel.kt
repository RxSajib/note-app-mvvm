package com.example.todoapp.Repository

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import com.example.todoapp.NoteModel
import com.example.todoapp.Utils.NoteDatabase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch


class ViewModel(application: Application) : AndroidViewModel(application) {

    private val readAllNote : LiveData<List<NoteModel>>
    private val repository : Repository

    init {
        val userdao = NoteDatabase.getDatabase(application).dao()
        repository = Repository(userdao)
        readAllNote = repository.readallnote
    }

    fun AddNote(noteModel: NoteModel){
        viewModelScope.launch(Dispatchers.IO) {
            repository.AddNote(noteModel)
        }
    }

    fun GetAllNote() : LiveData<List<NoteModel>>{
        return repository.GetAllNote();
    }

    fun UpdateNote(State : Boolean, ID : Int){
        viewModelScope.launch(Dispatchers.IO) {
            repository.UpdateNote(State, ID)
        }
    }
}