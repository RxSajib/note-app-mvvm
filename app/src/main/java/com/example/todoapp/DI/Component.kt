package com.example.todoapp.DI

import android.content.Context
import com.example.todoapp.Repository.Repository
import dagger.BindsInstance
import dagger.Component
import dagger.Module
import javax.inject.Singleton

@Component
@Singleton

interface Component {

    fun InjectNote(note: com.example.todoapp.UI.Note)

}