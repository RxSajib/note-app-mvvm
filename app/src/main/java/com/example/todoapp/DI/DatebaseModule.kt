package com.example.todoapp.DI

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import com.example.todoapp.Utils.NoteDatabase
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class DatebaseModule {

    @Singleton
    @Provides
    fun proverdatabase(context: Context) : NoteDatabase{
        return Room.databaseBuilder(context, NoteDatabase::class.java, "bd")
            .fallbackToDestructiveMigration()
            .build()
    }
}