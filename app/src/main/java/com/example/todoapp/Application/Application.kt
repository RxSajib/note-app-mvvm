package com.example.todoapp.Application

import android.app.Application
import androidx.appcompat.app.AppCompatDelegate
import com.example.todoapp.DI.Component
import com.example.todoapp.DI.DaggerComponent
import dagger.internal.DaggerCollections
import javax.inject.Singleton

@Singleton
class Application : Application() {

    lateinit var component: Component


    override fun onCreate() {
        super.onCreate()
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);

        component = DaggerComponent.create()
    }
}